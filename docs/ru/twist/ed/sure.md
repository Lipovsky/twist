# Twist-_ed_

## Файберы

Для тестирования кода, в котором пользователь использует stackful файберы, требуется аннотировать их стеки и переключения контекста.

### Sure

Поддержка для библиотеки [Sure](https://gitlab.com/Lipovsky/sure).

Включается через опцию CMake `TWIST_ED_SURE=ON`

Используйте:
- `twist::ed::sure::ExecutionContext` (заголовок `twist/ed/sure/context.hpp`)
- Стеки из `twist/ed/sure/stack/`:
  - `twist::ed::sure::GuardedMmapStack` (`mmap.hpp`)
  - `twist::ed::sure::NewStack` (`new.hpp`)


### Assist

Twist не обязывает пользователя использовать конкретную библиотеку корутин / файберов / переключения контекста.

В общем виде корутины / файберы поддержаны через [аннотации](/docs/ru/twist/assist.md) (см. пункт `Fiber`).
