# Twist-_ed_

## Futex

Заголовочный файл: `twist/ed/wait/futex.hpp`

Для блокирующего ожидания на атомиках нужно использовать
свободные функции из пространства имен `twist::ed::futex::`:
- `Wait` / `WaitTimed`
- `PrepareWake` + `Wake{One,All}`

[Реализация](https://gitlab.com/Lipovsky/futex_like)

### Пример

[examples/futex](/examples/futex/one_shot_event.hpp)

```cpp
class OneShotEvent {
 public:
  void Wait() {
    if (fired_.load() == 0) {
      // Паркуем текущий поток в очереди фьютекса для `fired_`, 
      // _если_ в атомике находится значение 0
      twist::ed::futex::Wait(fired_, /*old=*/0);
    }  
  }

  void Fire() {
    // NB: _До_ записи в fired_
    auto wake_key = twist::ed::futex::PrepareWake(fired_);

    fired_.store(1);
    // Будим все потоки, ждущие в futex::Wait на fired_
    twist::ed::futex::WakeAll(wake_key);
  }

 private:
  // Wait работает только с atomic-ом для типа uint32_t
  twist::ed::std::atomic<uint32_t> fired_{0};
};
```

### Операции

Зафиксируем `atomic<uint32_t>`, операции поддержаны только для него. 

(и для половин `atomic<uint64_t>`, см. отдельный пункт ниже)

##### Обозначения

Обозначим через `SysWait` и `SysWake` системные вызовы для парковки / пробуждения потока в данной операционной системе.

Обозначим через `Loc(atom)` адрес атомика в памяти.

#### Wait

```cpp
uint32_t Wait(atomic<uint32_t>& atom, uint32_t old, std::memory_order mo = std::memory_order::seq_cst) {
  uint32_t curr;
  
  do {
    SysWait(Loc(atom), old);
  } while ((curr = atom.load(mo)) == old);
  
  return curr;  // Новое значение
}
```

#### Wake

Протокол `Wake` – двухфазный:

1) Сначала (**до** записи в атомик, которая предшествует пробуждению) с помощью `PrepareWake` фиксируется ключ (`WakeKey`) для адресации системной очереди ожидания, связанной с атомиком (фактически – адрес атомика в памяти):

```cpp
WakeKey PrepareWake(atomic<uint32_t>& atom) {
  return {Loc(atom)};
}
````

`WakeKey` – _trivially copyable_

2) Затем (**после** записи в атомик) вызывается `Wake{One,All}` с адресом, зафиксированном на первом шаге, этот вызов будит ждущие потоки.

```cpp
void WakeOne(WakeKey key) {
  SysWake(key.loc, 1);
}
```

Обратите внимание: `PrepareWake` следует выполнять **до записи** в атомик.

#### WaitTimed

С помощью `futex::WaitTimed` можно встать в очередь ожидания с таймаутом:

```cpp
auto [woken, new_value] = futex::WaitTimed(state, kNotReady, 1s);
```

Если по истечении таймаута поток не был разбужен вызовом `WakeOne` / `WakeAll`, то он сам покинет очередь ожидания фьютекса.

Вызов возвращает пару (`woken`, `new_value`).

`woken` будет равен
- `true`, если 1) поток был разбужен с помощью вызовов `WakeOne` / `WakeAll` **или** 2) произошел spurious wakeup
- `false`, если истек таймаут

Между таймаутом и явным пробуждением есть гонка, так что `false` означает **лишь** то, что таймаут **точно истек**.
Был ли поток разбужен по таймеру или явным `Wake{One, All}` – неизвестно.

Если `woken == true`, то в `new_value` будет находиться новое значение атомика (отличное от `old`).

## `atomic<uint64_t>`

Функции `Wait`, `WaitTimed` и `PrepareWake` могут работать не только с `atomic_uint32_t`,
но и отдельно с младшей / старшей половиной `atomic_uint64_t`.

Ссылку на нужную половину `atomic_uint64_t` можно получить с помощью функций `RefLowHalf` / `RefHighHalf`.

Тип ссылки:
- `AtomicUint64LowHalfRef` для `RefLowHalf`
- `AtomicUint64HighHalfRef` для `RefHighHalf`

Ссылки – _trivially copyable_.

Вызов `Wait` на половине 64-битного атомика возвращает его **полное** значение.

См. https://lkml.org/lkml/2008/5/31/2

### Пример

```cpp
// Будем использовать старшие биты state для хранения флага
twist::ed::std::atomic_uint64_t state{0};

const uint64_t kSetFlag = (uint64_t)1 << 32;

// Ссылка для ожидания
auto flag_ref = twist::ed::futex::RefHighHalf(state);

twist::ed::std::thread t([&, flag_ref] {
  auto wake_key = twist::ed::futex::PrepareWake(flag_ref);
  state.fetch_or(kSetFlag);
  twist::ed::futex::WakeOne(wake_key);
});

// Wait возвращает новое значение атомика (целиком)
uint64_t s = twist::ed::futex::Wait(flag_ref, 0);

auto f = s >> 32;
assert(f == 1);

t.join();
```

## Замечания

#### Платформы

Имя _futex_ используется как собирательное для обозначения механизма блокирующего ожидания, который предоставляет
конкретная операционная система / платформа:

- [`futex`](https://github.com/torvalds/linux/blob/master/kernel/futex/waitwake.c) для Linux,
- [`ulock`](https://github.com/apple-oss-distributions/xnu/blob/main/bsd/kern/sys_ulock.c) для Darwin,
- [`WaitOnAddress`](https://learn.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-waitonaddress) для Windows.

### `atomic::wait`

По умолчанию Twist (намеренно) не поддерживает методы [`wait`](https://en.cppreference.com/w/cpp/atomic/atomic/wait) / [`notify`](https://en.cppreference.com/w/cpp/atomic/atomic/notify_one) у `atomic`:

- они доступны, но очень неэффективно реализуются для 64-битных типов
- нотификацией невозможно корректно пользоваться даже в очень простых сценариях синхронизации