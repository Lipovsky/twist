# Twist-_ed_

## `SpinWait`

Заголовочный файл: `<twist/ed/wait/spin.hpp>`

Предназначен для _адаптивного_ _активного ожидания_ (_busy waiting_) в спинлоках и мьютексах.

[Реализация](https://gitlab.com/Lipovsky/spin_wait)

### Пример

[examples/spin](/examples/spin/tas_spinlock.hpp)

```cpp
void SpinLock::Lock() {
  // Одноразовый!
  // Для каждого нового цикла ожидания в каждом потоке 
  // нужно заводить собственный экземпляр SpinWait
  twist::ed::SpinWait spin_wait;
  while (locked_.exchange(true)) {
    // У SpinWait определен operator()
    spin_wait();  // Exponential backoff
  }
}
```

Обращение к `SpinWait` помещает в цикл попыток _задержку_ (_backoff_) и наращивает ее по мере накопления
неудачных попыток.

См. [What is Backoff For?](https://brooker.co.za/blog/2022/08/11/backoff.html)

В реализации могут использоваться:
- [`pause`](https://c9x.me/x86/html/file_module_x86_id_232.html) или аналогичная инструкция
- [`std::this_thread::yield`](https://en.cppreference.com/w/cpp/thread/yield)
- [`nanosleep`](https://man7.org/linux/man-pages/man2/nanosleep.2.html)


### `ConsiderParking`

`SpinWait` может подсказать потоку, что активное ожидание затянулось и стоит рассмотреть парковку:

```cpp
void Mutex::Lock() {
  twist::ed::SpinWait spin_wait;
  
  while (!TryLock()) {
    if (spin_wait.ConsiderParking()) {
      ParkThisWaiter();  // ~ Запарковать текущий поток на фьютексе
    } else {
      spin_wait();  // Активное ожидание
    }
  }
}
```

Методу `ConsiderParking` допускается вернуть `true` сразу же, не предложив ни одной итерации активного ожидания. 

#### `KeepSpinning`

Цикл выше можно написать зеркально, используя метод `KeepSpinning`:

```cpp
bool SpinWait::KeepSpinning() const noexcept {
  return !ConsiderParking();
}
```
