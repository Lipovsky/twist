# Сборка

| `constexpr` флаг (`twist/build.hpp`) | Compile definition | Описание |
| --- | --- | --- |
| `twist::build::kSim` | `__TWIST_BUILD_SIM__` | Детерминированная симуляция |
| `kIsolatedSim` | `__TWIST_BUILD_ISOLATED_SIM__` | Детерминированная симуляция с изоляцией памяти |
| `kThreads` | `__TWIST_BUILD_THREADS__` | Потоки |
| `kPlain` | `__TWIST_BUILD_PLAIN__` | Сборка без вмешательства Twist |
| `kTwisted` | `__TWIST_BUILD_TWISTED__` или `__TWISTED__` | Сброка для тестов: потоки с внедрением сбоев или детерминированная симуляция |
