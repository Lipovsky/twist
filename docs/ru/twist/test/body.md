# Test

Утилиты для описания тестовых сценариев.

Должны использоваться **только в коде тестов**. Не должны использоваться в коде пользователя.

Директория: `twist/test/body/`

## `Plate`

Заголовок: `twist/test/body/plate.hpp`

[Dining philosophers problem](https://en.wikipedia.org/wiki/Dining_philosophers_problem)

Разделяемый ресурс, обращаться к которому можно только эксклюзивно.

```cpp
mutex.lock();
plate.Access();
mutex.unlock();
```

## `Either`

Заголовок: `twist/test/body/either.hpp`

Развилка в тестовом сценарии:

```cpp
if (twist::test::body::Either()) {
  mutex.lock();
  {
    // cs
  }
  mutex.unlock();
} else {
  if (mutex.try_lock()) {
    {
      // cs
    }
    mutex.unlock();
  }
}
```

Версия с числом альтернатив:

```cpp
switch (twist::test::body::Either(3)) {
  case 0:
    // Alt 1
    break;
  case 1:
    // Alt 2
    break;
  case 2:
    // Alt 3
    break;
};
```

При нагрузочном тестировании с потоками или при использовании рандомизированных планировщиков
в симуляции ветка выбирается случайно.

При переборе исполнений с model checker-ом (планировщик `Dfs`) перебираются 
**обе** (**все**) ветки. 

Эквивалент для `either` из [PlusCal](https://lamport.azurewebsites.net/tla/p-manual.pdf).

## `WaitGroup`

Заголовок: `twist/test/body/wg.hpp`

Группа потоков

```cpp
MutexToTest mutex;
twist::test::body::Plate plate;

twist::test::body::WaitGroup wg;

// Добавляем 3 потока, каждый из которых исполняет заданную лямбду
// Вызывать `Add` можно несколько раз
wg.Add(3, [&] {
  mutex.lock();
  plate.Access();
  mutex.unlock();
});

// Запускаем потоки и дожидаемся их завершения
wg.Join();
```

Лямбда, которую будет исполнять поток, может опционально получать индекс (отсчитывая от 0) потока:

```cpp
wg.Add(4, [](size_t index) {
  //
});
```

## Lock-Free

Заголовок: `twist/test/body/lock_free.hpp`

Подсказки для активации проверки гарантии lock-freedom.

```cpp
LockFreeStack<Unit> stack;

twist::test::body::WaitGroup wg;
wg.Add(3, [&stack] {
  // Скоуп, в котором рантайм twist может останавливать потоки
  twist::test::body::LockFreeScope lf;
  
  stack.Push({});
  // Сообщаем рантайму о прогрессе, т.е. о завершении вызова
  twist::test::body::Progress();
  
  stack.TryPop();
  twist::test::body::Progress();
})
```

## `Message`

Заголовок: `twist/test/body/message.hpp`

`Message<T>` – иммутабельные данные для тестирования синхронизации в сценариях message passing

```cpp
using TestMessage = twist::test::body::Message<int>;

LockFreeQueue<TestMessage> chan;

// Поток-producer
chan.Push(TestMessage::New(1));

// Поток-consumer
if (auto message = chan.TryPop()) {  // Пусть метод TryPop у LockFreeQueue<T> возвращает std::optional<T>
  message->Read();  // Проверка happens-before (в симуляции Twist или с помощью ThreadSanitizer)
}
```