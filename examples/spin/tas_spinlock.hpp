#pragma once

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/wait/spin.hpp>

//////////////////////////////////////////////////////////////////////

// Naive Test-and-Set (TAS) spinlock

// Correctly implementing a spinlock in C++
// https://rigtorp.se/spinlock/

class SpinLock {
 public:
  void Lock() {
    twist::ed::SpinWait spin_wait;
    while (locked_.exchange(true)) {
      spin_wait();
    }
  }

  void Unlock() {
    locked_.store(false);
  }

 private:
  twist::ed::std::atomic<bool> locked_{false};
};
