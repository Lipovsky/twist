#include "one_shot_event.hpp"

#include <twist/cross.hpp>

#include <twist/ed/std/thread.hpp>
#include <twist/ed/fmt/print.hpp>

#include <chrono>

using namespace std::chrono_literals;


int main() {
  twist::cross::Run([] {
    std::string data;
    OneShotEvent flag;

    twist::ed::std::thread producer([&] {
      twist::ed::std::this_thread::sleep_for(3s);

      data = "Hello";
      flag.Set();
    });

    flag.Wait();

    twist::ed::fmt::println("Produced: {}", data);

    producer.join();
  });

  return 0;
}
