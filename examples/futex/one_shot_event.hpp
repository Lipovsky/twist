#pragma once

#include <twist/ed/std/atomic.hpp>
#include <twist/ed/wait/futex.hpp>

class OneShotEvent {
  struct States {
    enum _ : uint32_t {
      NotSet = 0,
      Set = 1
    };
  };

 public:
  void Wait() {
    if (state_.load() != States::Set) {
      twist::ed::futex::Wait(state_, /*old=*/States::NotSet);
    }
  }

  // One-shot
  void Set() {
    // NB: before store
    auto wake_key = twist::ed::futex::PrepareWake(state_);

    state_.store(States::Set);
    twist::ed::futex::WakeAll(wake_key);
  }

 private:
  twist::ed::std::atomic<uint32_t> state_{States::NotSet};
};
