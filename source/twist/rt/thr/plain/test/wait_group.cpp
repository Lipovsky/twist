#include <twist/rt/thr/test/wait_group.hpp>

namespace twist::rt::thr {

namespace test {

void WaitGroup::ThreadEnter() {
  // Nop
}

void WaitGroup::ThreadExit() {
  // Nop
}

}  // namespace test

}  // namespace twist::rt::thr
