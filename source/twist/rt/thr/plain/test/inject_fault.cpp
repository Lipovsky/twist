#include <twist/rt/thr/test/inject_fault.hpp>

namespace twist::rt::thr {

namespace test {

void InjectFault(wheels::SourceLocation) {
  // No-op
}

}  // namespace test

}  // namespace twist::test
