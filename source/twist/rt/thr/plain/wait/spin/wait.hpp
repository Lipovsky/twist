#pragma once

#if defined(__TWIST_THR_SINGLE_CORE__)

#include "cores/single.hpp"

namespace twist::rt::thr {

using cores::single::SpinWait;
using cores::single::CpuRelax;

}  // namespace twist::rt::thr

#else

#include "cores/multi.hpp"

namespace twist::rt::thr {

using cores::multi::SpinWait;
using cores::multi::CpuRelax;

}  // namespace twist::rt::thr

#endif
