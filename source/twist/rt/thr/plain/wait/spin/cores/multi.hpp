#pragma once

#include <sw/spin_wait.hpp>
#include <sw/spin_loop_hint.hpp>

namespace twist::rt::thr {

namespace cores::multi {

// SpinWait

class [[nodiscard]] SpinWait {
 public:
  SpinWait() = default;

  // Non-copyable
  SpinWait(const SpinWait&) = delete;
  SpinWait& operator=(const SpinWait&) = delete;

  // Non-movable
  SpinWait(SpinWait&&) = delete;
  SpinWait& operator=(SpinWait&&) = delete;

  void Wait() {
    impl_.Wait();
  }

  void Spin() {
    Wait();
  }

  void operator()() {
    Wait();
  }

  [[nodiscard]] bool ConsiderParking() const {
    return impl_.ConsiderParking();
  }

  [[nodiscard]] bool KeepSpinning() const {
    return impl_.KeepSpinning();
  }

 private:
  sw::SpinWait impl_;
};

// CpuRelax

inline void CpuRelax() {
  sw::SpinLoopHint();
}

}  // namespace cores::multi

}  // namespace twist::rt::thr
