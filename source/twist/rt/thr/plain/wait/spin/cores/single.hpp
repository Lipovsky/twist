#pragma once

#include <thread>

namespace twist::rt::thr {

namespace cores::single {

// SpinWait

class [[nodiscard]] SpinWait {
 public:
  SpinWait() = default;

  // Non-copyable
  SpinWait(const SpinWait&) = delete;
  SpinWait& operator=(const SpinWait&) = delete;

  // Non-movable
  SpinWait(SpinWait&&) = delete;
  SpinWait& operator=(SpinWait&&) = delete;


  void Wait() {
    ++yield_count_;
    std::this_thread::yield();
  }

  void Spin() {
    Wait();
  }

  [[nodiscard]] bool ConsiderParking() const {
    static const size_t kYieldLimit = 4;  // Arbitrary

    return yield_count_ > kYieldLimit;
  }

  [[nodiscard]] bool KeepSpinning() const {
    return !ConsiderParking();
  }

  void operator()() {
    Wait();
  }

 private:
  size_t yield_count_ = 0;
};

// CpuRelax

inline void CpuRelax() {
  // No-op
}

}  // namespace cores::single

}  // namespace twist::rt::thr
