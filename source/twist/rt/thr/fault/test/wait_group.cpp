#include <twist/rt/thr/test/wait_group.hpp>

#include <twist/rt/thr/fault/affinity/affinity.hpp>
#include <twist/rt/thr/fault/adversary/adversary.hpp>

namespace twist::rt::thr {

namespace test {

void WaitGroup::ThreadEnter() {
  twist::test::SetThreadAffinity();
  fault::Adversary()->Enter();
}

void WaitGroup::ThreadExit() {
  fault::Adversary()->Exit();
}

}  // namespace test

}  // namespace twist::rt::thr
