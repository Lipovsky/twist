#include <twist/rt/thr/test/inject_fault.hpp>

#include <twist/rt/thr/fault/adversary/inject_fault.hpp>

namespace twist::rt::thr {

namespace test {

void InjectFault(wheels::SourceLocation) {
  rt::thr::fault::InjectFault();
}

}  // namespace test

}  // namespace twist::rt::thr
