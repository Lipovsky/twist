#pragma once

#include <cstdint>

namespace twist::rt::thr {

namespace test {

inline void SwitchTo(std::uint8_t) {
  static_assert(false, "Not supported in build::kThreads");
}

}  // namespace test

}  // namespace twist::rt::thr
