#pragma once

#include <sure/stack/mmap.hpp>

namespace twist::rt::thr {

namespace sure::stack {

using ::sure::stack::GuardedMmapStack;

}  // namespace sure::stack

}  // namespace twist::rt::thr
