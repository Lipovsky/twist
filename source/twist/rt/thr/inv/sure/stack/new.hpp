#pragma once

#include <sure/stack/new.hpp>

namespace twist::rt::thr {

namespace sure::stack {

using ::sure::stack::NewStack;

}  // namespace sure::stack

}  // namespace twist::rt::thr
