#pragma once

#include <sure/stack/new.hpp>

namespace twist::rt::sim {

namespace user::library {

namespace sure::stack {

using ::sure::stack::NewStack;

}  // namespace sure::stack

}  // namespace user::library

}  // namespace twist::rt::sim
