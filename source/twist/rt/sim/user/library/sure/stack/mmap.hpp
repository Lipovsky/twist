#pragma once

#include <cstddef>  // std::byte
#include <span>

namespace twist::rt::sim {

namespace user::library {

namespace sure::stack {

class MmapLikeStack {
  static const size_t kPageSize = 4096;

 private:
  struct NewAlloc {
    std::byte* start;
    size_t size;
  };

 public:
  MmapLikeStack() = delete;

  // Non-copyable
  MmapLikeStack(const MmapLikeStack&) = delete;
  MmapLikeStack& operator=(const MmapLikeStack&) = delete;

  // Move-constructible
  MmapLikeStack(MmapLikeStack&& that)
      : alloc_(std::exchange(that.alloc_, {nullptr, 0})) {
  }

  // Move-assignable
  MmapLikeStack& operator=(MmapLikeStack&& that) {
    Swap(that);
    that.Reset();
    return *this;
  }

  static MmapLikeStack AllocateBytes(size_t at_least) {
    size_t size = RoundUpToPages(at_least);
    return MmapLikeStack{NewAlloc{new std::byte[size], size}};
  }

  void* LowerBound() const noexcept {
    return alloc_.start;
  }

  size_t AllocationSize() const noexcept {
    return alloc_.size;
  }

  std::span<std::byte> MutView() noexcept {
    return {alloc_.start, alloc_.size};
  }

  ~MmapLikeStack() {
    Reset();
  }

  static size_t PageSize() {
    return kPageSize;
  }

 private:
  explicit MmapLikeStack(NewAlloc alloc)
      : alloc_(alloc) {
  }

  static size_t RoundUpToPages(size_t at_least) {
    const size_t page_size = PageSize();

    size_t pages = at_least / page_size;
    if (at_least % page_size != 0) {
      ++pages;
    }

    return pages * page_size;
  }

  void Swap(MmapLikeStack& that) {
    std::swap(alloc_, that.alloc_);
  }

  void Reset() {
    if (alloc_.start != nullptr) {
      delete[] alloc_.start;
      alloc_ = {nullptr, 0};
    }
  }

 private:
  NewAlloc alloc_;
};

using GuardedMmapStack = MmapLikeStack;

}  // namespace sure::stack

}  // namespace user::library

}  // namespace twist::rt::sim
