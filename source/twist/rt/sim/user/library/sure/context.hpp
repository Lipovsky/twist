#pragma once

#include <sure/context.hpp>

#include <twist/rt/sim/user/fiber/assist.hpp>

namespace twist::rt::sim {

namespace user::library {

namespace sure {

class ExecutionContext {
 public:
  ExecutionContext() = default;

  // Non-copyable
  ExecutionContext(const ExecutionContext&) = delete;
  ExecutionContext& operator=(const ExecutionContext&) = delete;

  // Non-movable
  ExecutionContext(ExecutionContext&&) = delete;
  ExecutionContext& operator=(ExecutionContext&&) = delete;

  void Setup(::sure::StackView stack, ::sure::ITrampoline* trampoline) {
    ctx_.Setup(stack, trampoline);

    fiber::NewFiber(&new_fiber_, stack);
    fiber_ = &new_fiber_;
  }

  void SwitchTo(ExecutionContext& target) {
    fiber_ = fiber::SwitchToFiber(target.fiber_);
    ctx_.SwitchTo(target.ctx_);
  }

  [[noreturn]] void ExitTo(ExecutionContext& target) {
    fiber_ = fiber::SwitchToFiber(target.fiber_);
    ctx_.ExitTo(target.ctx_);
  }

  void* StackPointer() const noexcept {
    return ctx_.StackPointer();
  }

 private:
  ::sure::ExecutionContext ctx_;
  fiber::Fiber new_fiber_;
  fiber::Fiber* fiber_ = nullptr;
};

}  // namespace sure

}  // namespace user::library

}  // namespace twist::rt::sim
