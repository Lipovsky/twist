#pragma once

#include <cstdint>

namespace twist::rt::sim {

namespace user::test {

void SwitchTo(std::uint8_t thread_id);

}  // namespace user::test

}  // namespace twist::rt::sim
