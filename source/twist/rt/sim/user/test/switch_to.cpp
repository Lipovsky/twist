#include "switch_to.hpp"

#include <twist/rt/sim/user/syscall/switch_to.hpp>

namespace twist::rt::sim {

namespace user::test {

void SwitchTo(std::uint8_t thread_id) {
  syscall::SwitchTo(thread_id);
}

}  // namespace user::test

}  // namespace twist::rt::sim
