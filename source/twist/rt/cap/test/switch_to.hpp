#pragma once

#if defined(__TWIST_SIM__)

#include <twist/rt/sim/user/test/switch_to.hpp>

namespace twist::rt::cap {

namespace test {

using rt::sim::user::test::SwitchTo;

}  // namespace test

}  // namespace twist::rt::cap

#else

#include <twist/rt/thr/test/switch_to.hpp>

namespace twist::rt::cap {

namespace test {

using rt::thr::test::SwitchTo;

}  // namespace test

}  // namespace twist::rt::cap

#endif
