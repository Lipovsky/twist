#pragma once

#if 1

#if defined(__TWIST_SIM__)

#include <twist/rt/sim/user/library/sure/stack/mmap.hpp>

namespace twist::rt::cap {

namespace sure::stack {

using sim::user::library::sure::stack::GuardedMmapStack;

}  // namespace sure::stack

}  // namespace twist::rt::cap

#else

#include <twist/rt/thr/inv/sure/stack/mmap.hpp>

namespace twist::rt::cap {

namespace sure::stack {

using thr::sure::stack::GuardedMmapStack;

}  // namespace sure::stack

}  // namespace twist::rt::cap

#endif

#else

#error "Sure library is not supported"

#endif
