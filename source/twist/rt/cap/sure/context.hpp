#pragma once

#if 1

#if defined(__TWIST_SIM__)

#include <twist/rt/sim/user/library/sure/context.hpp>

namespace twist::rt::cap {

namespace sure {

using sim::user::library::sure::ExecutionContext;

}  // namespace sure

}  // namespace twist::rt::cap

#else

#include <twist/rt/thr/inv/sure/context.hpp>

namespace twist::rt::cap {

namespace sure {

using thr::sure::ExecutionContext;

}  // namespace sure

}  // namespace twist::rt::cap

#endif

#else

#error "Sure library is not supported"

#endif
