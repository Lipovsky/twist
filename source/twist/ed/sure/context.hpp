#pragma once

#include <twist/rt/cap/sure/context.hpp>

namespace twist::ed {

namespace sure {

using rt::cap::sure::ExecutionContext;

}  // namespace sure

}  // namespace twist::ed
