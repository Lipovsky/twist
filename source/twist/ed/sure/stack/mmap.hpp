#pragma once

#include <twist/rt/cap/sure/stack/mmap.hpp>

namespace twist::ed {

namespace sure::stack {

using rt::cap::sure::stack::GuardedMmapStack;

}  // namespace sure::stack

}  // namespace twist::ed
