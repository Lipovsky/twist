#pragma once

#include <twist/rt/cap/sure/stack/new.hpp>

namespace twist::ed {

namespace sure::stack {

using rt::cap::sure::stack::NewStack;

}  // namespace sure::stack

}  // namespace twist::ed
