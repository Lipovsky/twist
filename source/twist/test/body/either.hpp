#pragma once

/*
 * bool twist::test::body::Either();
 *
 * Usage:
 *
 * if (twist::test::body::Either()) {
 *   stack.Push(1);
 * } else {
 *   stack.TryPop();
 * }
 *
 */

#include <twist/rt/cap/test/either.hpp>

namespace twist::test::body {

using twist::rt::cap::test::Either;

}  // namespace twist::test::body
