#pragma once

/*
 * Manually inject fault / context switch
 *
 * twist::test::body::InjectFault();
 *
 */

#include <twist/rt/cap/test/inject_fault.hpp>

namespace twist::test::body {

using twist::rt::cap::test::InjectFault;

}  // namespace twist::test::body
