#pragma once

/*
 * // [0, alts)
 * size_t twist::test::body::Choice(alts);
 *
 * // [lo, hi)
 * size_t twist::test::body::Choice(lo, hi);
 *
 */

#include <twist/rt/cap/test/choice.hpp>

namespace twist::test::body {

using twist::rt::cap::test::Choice;

}  // namespace twist::test::body
