#pragma once

#include <twist/rt/cap/test/checksum.hpp>

namespace twist::test::body {

using twist::rt::cap::test::CheckSum;

}  // namespace twist::test::body
