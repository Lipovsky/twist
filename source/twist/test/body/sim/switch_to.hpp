#pragma once

#include <twist/rt/cap/test/switch_to.hpp>

namespace twist::test::body {

namespace sim {

using rt::cap::test::SwitchTo;

}  // namespace sim

}  // namespace twist::test::body
