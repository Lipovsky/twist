#pragma once

#include <twist/test/body/plate.hpp>

namespace twist::test::body {

class CriticalSection {
 public:
  void operator()() {
    shared_plate_.Access();
  }

 private:
  Plate shared_plate_;
};

}  // namespace twist::test::body
