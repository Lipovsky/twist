#pragma once

#include "loc.hpp"

namespace twist::trace {

struct EventCtx {
  Loc loc;
};

}  // namespace twist::trace
