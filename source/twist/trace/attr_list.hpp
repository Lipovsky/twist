#pragma once

#include "attr.hpp"
#include "attr/unit.hpp"

#include <cassert>

namespace twist::trace {

class AttrList : public wheels::IntrusiveList<AttrBase> {
 public:
  ~AttrList() {
    UnlinkAll();
  }
};

}  // namespace twist::trace
