#pragma once

#include "attr_value.hpp"

#include <wheels/intrusive/list.hpp>

namespace twist::trace {

struct IAttr {
  virtual const char* GetName() const noexcept = 0;
  virtual AttrValue GetValue() const noexcept = 0;
};

struct AttrBase : IAttr,
                  wheels::IntrusiveListNode<AttrBase> {

  ~AttrBase() {
    if (IsLinked()) {
      Unlink();
    }
  }
};

}  // namespace twist::trace
