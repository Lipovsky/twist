#pragma once

#include "fmt.hpp"

namespace twist::trace {

namespace attr {

template <size_t BufSize = 64>
struct SnippetLineComment : Fmt<BufSize> {
  template <typename ... Args>
  SnippetLineComment(::fmt::format_string<Args...> format_str, Args&&... args)
      : Fmt<BufSize>("twist.snippet_line_comment", format_str, ::std::forward<Args>(args)...) {
  }
};

}  // namespace attr

}  // namespace twist::trace
