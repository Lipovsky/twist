#pragma once

#include "../runtime.hpp"

#include "../event_context.hpp"

#include "detail/temp_message.hpp"

#include <type_traits>

namespace twist::trace {

namespace detail {

template <typename... Args>
void LogSimple(Scope* scope, EventCtx ctx, ::fmt::format_string<Args...> format_str,
                  Args&&... args) {
  if (scope != nullptr) {
    if (scope->IsEventVisible()) {
      Event event{scope, ctx.loc,
                  trace::detail::FormatTempMessage(format_str, std::forward<Args>(args)...)};
      rt::Trace(&event);
    }
  }
}

}  // namespace detail

}  // namespace twist::trace

#define TWIST_LOG(...) ::twist::trace::detail::LogSimple( \
    twist::trace::rt::TryCurrentScope(),                  \
    {::twist::trace::Loc::Current()},                     \
    __VA_ARGS__)

#define TWIST_LOG_HERE TWIST_LOG("Here")
