#pragma once

#include "../event.hpp"
#include "../runtime.hpp"

#include "detail/temp_message.hpp"

#include <string_view>

namespace twist::trace {

namespace detail {

class LogEvent {
 public:
  explicit LogEvent(Scope* hint) {
    event_.scope = (hint != nullptr)
                       ? hint
                       : rt::TryCurrentScope();
  }

  // Non-copyable
  LogEvent(const LogEvent&) = delete;
  LogEvent& operator=(const LogEvent&) = delete;

  // Non-movable
  LogEvent(LogEvent&&) = delete;
  LogEvent& operator=(LogEvent&&) = delete;

  template <typename... Args>
  LogEvent& Message(::fmt::format_string<Args...> format_str, Args&&... args) {
    event_.message = detail::FormatTempMessage(format_str, std::forward<Args>(args)...);
    return *this;
  }

  LogEvent& Message(std::string_view m) {
    event_.message = m;
    return *this;
  }

  LogEvent& Here(Loc loc = Loc::Current()) {
    event_.loc = loc;
    return *this;
  }

  LogEvent& Inline() {
    event_.loc = event_.scope->GetLoc();
    return *this;
  }

  LogEvent& At(Loc loc) {
    event_.loc = loc;
    return *this;
  }

  LogEvent& Attr(AttrBase& attr) {
    event_.LinkAttr(attr);
    return *this;
  }

  void Log() {
    if (event_.scope->IsEventVisible()) {
      rt::Trace(&event_);
    }
  }

 private:
  Event event_;
};

}  // namespace detail

// Unsafe
inline detail::LogEvent Ev(Scope& hint) {
  return detail::LogEvent{&hint};
}

inline detail::LogEvent Ev() {
  return detail::LogEvent{nullptr};
}

}  // namespace twist::trace
