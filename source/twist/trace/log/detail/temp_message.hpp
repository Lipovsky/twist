#pragma once

#include <fmt/format.h>

#include <span>

namespace twist::trace {

namespace detail {

std::span<char> StaticMessageBuffer();

template <typename... Args>
std::string_view FormatTempMessage(::fmt::format_string<Args...> format_str,
                                  Args&&... args) {
  auto buf = StaticMessageBuffer();
  auto written = ::fmt::format_to_n(buf.data(), buf.size(), format_str,
                                    ::std::forward<Args>(args)...);
  return {buf.data(), written.size};
}

}  // namespace detail

}  // namespace twist::trace
