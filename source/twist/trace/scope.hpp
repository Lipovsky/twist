#pragma once

#include "domain.hpp"
#include "loc.hpp"
#include "context.hpp"
#include "attr.hpp"
#include "attr_list.hpp"
#include "id.hpp"

#include "attr/uint.hpp"

#include "event_context.hpp"

#include "log/detail/temp_message.hpp"

#include <type_traits>

namespace twist::trace {

class Scope {
 public:
  // Enter domain
  Scope(const Domain& domain, const char* name, Loc loc = Loc::Current());
  Scope(const Domain& domain, Loc loc = Loc::Current())
      : Scope(domain, loc.Function(), loc) {
  }

  // Inherit domain from the parent scope
  explicit Scope(const char* name, Loc loc = Loc::Current());
  explicit Scope(Loc loc = Loc::Current())
      : Scope(loc.Function(), loc) {
  }

  // Non-copyable
  Scope(const Scope&) = delete;
  Scope& operator=(const Scope&) = delete;

  // Non-movable
  Scope(Scope&&) = delete;
  Scope& operator=(Scope&&) = delete;

  ~Scope();

  static Scope* TryCurrent() noexcept;

  Id GetId() const noexcept {
    return id_;
  }

  const char* GetName() const noexcept {
    return name_;
  }

  Loc GetLoc() const noexcept {
    return loc_;
  }

  Domain* GetDomain() const noexcept {
    return domain_;
  }

  Scope* GetParent() const noexcept {
    return parent_;
  }

  Context GetContext() const noexcept {
    return context_;
  }

  bool IsRoot() const noexcept {
    return parent_ == nullptr;
  }

  Scope& LinkAttr(AttrBase& attr) noexcept {
    attrs_.PushBack(&attr);
    return *this;
  }

  const AttrList& GetAttrs() const noexcept {
    return attrs_;
  }

  int GetVisibility() const noexcept {
    return context_.visibility;
  }

  void SetVisibility(int val) noexcept {
    context_.visibility = val;
  }

  int GetEventVisibility() const noexcept;

  bool IsEventVisible() const noexcept {
    return GetEventVisibility() > 0;
  }

 private:
  const char* name_;
  const Loc loc_;
  Domain* domain_;
  Scope* parent_;
  Id id_;
  Context context_;
  AttrList attrs_;
};

}  // namespace twist::trace
