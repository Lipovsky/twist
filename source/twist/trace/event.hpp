#pragma once

#include "domain.hpp"
#include "scope.hpp"
#include "loc.hpp"
#include "attr.hpp"
#include "attr_list.hpp"

#include <string_view>

namespace twist::trace {

struct Event {
  Scope* scope;
  Loc loc;
  std::string_view message;
  AttrList attrs = {};

 public:
  Event& LinkAttr(AttrBase& attr) noexcept {
    attrs.PushBack(&attr);
    return *this;
  }

  const AttrList& GetAttrs() const noexcept {
    return attrs;
  }
};

}  // namespace twist::trace
