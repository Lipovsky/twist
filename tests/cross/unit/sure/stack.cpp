#include <twist/cross.hpp>
#include <twist/build.hpp>

#include <twist/ed/sure/stack/mmap.hpp>

#include <wheels/test/framework.hpp>

TEST_SUITE(SureStack) {
  SIMPLE_TEST(GuardedMmapStack) {
    twist::cross::Run([] {
      using Stack = twist::ed::sure::stack::GuardedMmapStack;

      auto s = Stack::AllocateBytes(5001);

      size_t page_size = Stack::PageSize();

      if constexpr (twist::build::kSim) {
        ASSERT_EQ(s.PageSize(), 4096);
      }

      ASSERT_TRUE(s.AllocationSize() % page_size == 0);
      ASSERT_TRUE(s.AllocationSize() > 5001);

      {
        auto s2 = std::move(s);
      }
    });
  }
}
