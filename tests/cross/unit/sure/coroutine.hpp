#pragma once

#include <twist/ed/sure/context.hpp>
#include <twist/ed/sure/stack/new.hpp>
#include <twist/ed/sure/stack/mmap.hpp>

#include <cstdlib>
#include <cassert>
#include <functional>

class Coroutine : private sure::ITrampoline {
  using Body = std::function<void()>;
  using Stack = twist::ed::sure::stack::GuardedMmapStack;

  static constexpr size_t kStackSize = 128 * 1024;

 public:
  explicit Coroutine(Body body)
      : body_(body),
        stack_{Stack::AllocateBytes(kStackSize)} {

    context_.Setup(stack_.MutView(), this);
  }

  Coroutine(const Coroutine&) = delete;
  Coroutine(Coroutine&&) = delete;

  void Resume() {
    assert(!IsDone());
    caller_context_.SwitchTo(context_);
  }

  void Suspend() {
    context_.SwitchTo(caller_context_);
  }

  bool IsDone() const noexcept {
    return done_;
  }

 private:
  void Run() noexcept override {
    try {
      body_();
    } catch (...) {
      std::abort();
    }

    done_ = true;
    context_.ExitTo(caller_context_);
  }

 private:
  Body body_;
  Stack stack_;
  twist::ed::sure::ExecutionContext context_;
  twist::ed::sure::ExecutionContext caller_context_;
  bool done_ = false;
};
